# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://MatejTheLes@bitbucket.org/MatejTheLes/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/MatejTheLes/stroboskop/commits/dac10bad5f0f12b972f64e11d402e12cb0e020d9

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/MatejTheLes/stroboskop/commits/5cf5e884cd6b99d83f380c5a45c7a9bc38b678fc

Naloga 6.3.2:
https://bitbucket.org/MatejTheLes/stroboskop/commits/93790a4e0cc2904ed75471170f9d6070f3d87139

Naloga 6.3.3:
https://bitbucket.org/MatejTheLes/stroboskop/commits/b8cf94e72efb34156f6de1a6d7b9b07fc3e9fbab

Naloga 6.3.4:
https://bitbucket.org/MatejTheLes/stroboskop/commits/943db1b7a7ec2ee61a4934772cbe56d301ed3b36

Naloga 6.3.5:

```
git checkout master
git merge izgled

```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/MatejTheLes/stroboskop/commits/d9fe62e876e026cf411d263e3211cffadeecfc39

Naloga 6.4.2:
https://bitbucket.org/MatejTheLes/stroboskop/commits/f0a6a9d1c0e6d248195e0df264a3fe7f69042dd1

Naloga 6.4.3:
https://bitbucket.org/MatejTheLes/stroboskop/commits/f97a8091460193ace9f936da438d19fc24987519

Naloga 6.4.4:
https://bitbucket.org/MatejTheLes/stroboskop/commits/e95307b9e77407233f73b72b4a473b0799c035da